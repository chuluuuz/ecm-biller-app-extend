import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import path from "path"; // Import path module

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  build: {
    outDir: "build", // Replace 'dist' with your desired output directory name
  },
  resolve: {
    alias: {
      "@pages": path.resolve(
        new URL(".", import.meta.url).pathname,
        "./src/pages"
      ),
      "@components": path.resolve(
        new URL(".", import.meta.url).pathname,
        "./src/components"
      ),

      // Add more aliases as needed
    },
  },
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
});
