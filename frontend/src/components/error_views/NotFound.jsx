import React from "react";

const NotFound = () => {
  return (
    <>
      <div style={{ textAlign: "center" }}>Page not found</div>
    </>
  );
};

export default NotFound;
