import { Space, Button, Dropdown, Card, Row, Col } from "antd";
import PageTitle from "./PageTitle";
import PageButtons from "./PageButtons";
import PropTypes from "prop-types";
const PageHeader = ({ pageTitle, pageIcon, pagePath, pageButtons }) => {
  return (
    <Row
      justify="space-between"
      style={{
        padding: "0px 10px 10px",
      }}
    >
      <PageTitle title={pageTitle} icon={pageIcon} path={pagePath} />
      {pageButtons && <PageButtons pageActionButtons={pageButtons} />}
    </Row>
  );
};
PageHeader.propTypes = {
  pageTitle: PropTypes.string,
  pageIcon: PropTypes.object,
  pagePath: PropTypes.string,
  pageButtons: PropTypes.object,
};
export default PageHeader;
