import * as XLSX from "xlsx";
import * as FileSaver from "file-saver";
import PropTypes from "prop-types";
function exportToXLSX(data, columns, filename) {
  // remove hidden columns

  const visibleColumns = columns; //.filter((column) => !column.hidden);
  const dataWithHeaders = [
    visibleColumns.map((column) => column.title),
    ...data.map((item) => columns.map((column) => item[column.dataIndex])),
  ];

  const worksheet = XLSX.utils.aoa_to_sheet(dataWithHeaders, {});
  const workbook = { Sheets: { data: worksheet }, SheetNames: ["data"] };
  const excelBuffer = XLSX.write(workbook, { bookType: "xlsx", type: "array" });
  const fileData = new Blob([excelBuffer], {
    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8",
  });
  FileSaver.saveAs(fileData, `${filename}.xlsx`);
}
exportToXLSX.propTypes = {
  pageActionButtons: PropTypes.object,
};
export default exportToXLSX;
