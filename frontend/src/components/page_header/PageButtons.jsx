import { PlusOutlined, DownloadOutlined } from "@ant-design/icons";
import { Row, Button, Tooltip } from "antd";
import exportToXLSX from "./PageExcelExport";
import PropTypes from "prop-types";
const PageButtons = ({ pageActionButtons }) => {
  const { dataExportName, dataSource, columns, onCreate } = pageActionButtons;

  return (
    <Row
      justify="space-between"
      align="bottom"
      style={{ gap: "8px" }}
      key={"pagebuttons"}
    >
      {onCreate && (
        <Tooltip title="Үүсгэх">
          <Button type="primary" size="small" onClick={() => onCreate()}>
            <PlusOutlined />
          </Button>
        </Tooltip>
      )}

      {dataSource && (
        <Tooltip title="Эксел экспорт">
          <Button
            type="primary"
            size="small"
            onClick={() => exportToXLSX(dataSource, columns, dataExportName)}
          >
            <DownloadOutlined />
          </Button>
        </Tooltip>
      )}
    </Row>
  );
};
PageButtons.propTypes = {
  pageActionButtons: PropTypes.object,
};
export default PageButtons;
