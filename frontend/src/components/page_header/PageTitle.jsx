import React from "react";
import { Space, Typography, Breadcrumb, Flex } from "antd";
import PropTypes from "prop-types";
const PageTitle = ({ title, icon, path }) => {
  return (
    <>
      <span style={{ fontWeight: "500" }}>{title}</span>

      {/* <span>
          <Breadcrumb>
            <Breadcrumb.Item>
              <span className="breadcrumb-title">{path}</span>
            </Breadcrumb.Item>
          </Breadcrumb>
        </span> */}
    </>
  );
};
PageTitle.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.icon,
  path: PropTypes.string,
};
export default PageTitle;
