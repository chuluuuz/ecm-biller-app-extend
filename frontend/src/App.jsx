import "./styles/index.scss";
import { useRoutes } from "react-router-dom";
import routes from "./pages/routes";
import { ConfigProvider, theme } from "antd";
import mnMN from "antd/lib/locale/mn_MN";
function App() {
  const { darkAlgorithm, compactAlgorithm } = theme;

  const mainRouters = useRoutes(routes);
  return (
    <ConfigProvider
      locale={mnMN}
      theme={{
        token: {
          fontSize: 12,
        },
        algorithm: darkAlgorithm,
        compactAlgorithm: compactAlgorithm,
      }}
    >
      {mainRouters}
    </ConfigProvider>
  );
}

export default App;
