import { NotFound } from "@components";
import Login from "./login/Login";
import ProtectedRoute from "./routes/ProtectedRoute";
import Biller from "./biller/Biller";

const routes = [
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/",
    element: (
      <ProtectedRoute>
        <Biller />
      </ProtectedRoute>
    ),
  },
  {
    path: "*",
    element: (
      <ProtectedRoute>
        <NotFound />
      </ProtectedRoute>
    ),
  },
];

export default routes;
