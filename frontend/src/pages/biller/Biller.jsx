import { Row, Table } from "antd";
import { useEffect, useState } from "react";
import { PageHeader } from "@components";
import BillerForm from "./BillerForm";
import axiosInstance from "../../api/axios";
const Biller = () => {
  const [dataSource, setDataSource] = useState([]);
  const [isCreateVisible, setIsCreateVisible] = useState(false);
  const [rowRecord, setRowRecord] = useState(false);
  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = async () => {
    try {
      const result = await axiosInstance.get("/api/biller");

      if (result.status === 200 && result?.data?.length > 0) {
        setDataSource(result?.data);
      }
    } catch (error) {
      setDataSource([]);
      console.error(error);
    }
  };
  const columns = [
    {
      key: "biller_code",
      title: "Биллер Код",
      dataIndex: "biller_code",
      hidden: false,
      ellipsis: true,
      width: 100,
      sorter: (a, b) => a.biller_code - b.biller_code, // Sort numerically
    },
    {
      key: "company_name",
      title: "Байгуулга",
      dataIndex: "company_name",
      hidden: false,
      ellipsis: true,
      width: 100,
      sorter: (a, b) => a.company_name.localeCompare(b.company_name), // Sort alphabetically
    },
    {
      key: "product",
      title: "Бүтээгдэхүүн",
      dataIndex: "product",
      hidden: false,
      ellipsis: true,
      width: 100,
      sorter: (a, b) => a.product.localeCompare(b.product), // Sort alphabetically
    },
    {
      key: "start_date",
      title: "Эхлэсэн огноо",
      dataIndex: "start_date",
      hidden: false,
      ellipsis: true,
      width: 100,
      sorter: (a, b) => a.start_date.localeCompare(b.start_date), // Sort alphabetically
    },
    {
      key: "end_date",
      title: "Дуусах огноо",
      dataIndex: "end_date",
      hidden: false,
      ellipsis: true,
      width: 100,
      sorter: (a, b) => a.end_date.localeCompare(b.end_date), // Sort alphabetically
    },
    // {
    //   key: "created_user",
    //   title: "Үүсгэсэн",
    //   dataIndex: "created_user",
    //   hidden: false,
    //   ellipsis: true,
    //   width: 100,
    //   sorter: (a, b) => a.product.localeCompare(b.product), // Sort alphabetically
    // },
  ];

  const onCreate = () => {
    setIsCreateVisible(true);
  };
  const onCloseModal = (isCloseFetch = null) => {
    setIsCreateVisible(false);
    isCloseFetch && fetchData();
  };
  const onHandleDoubleClick = (record) => {
    setIsCreateVisible(true);
    setRowRecord(record);
  };
  return (
    <>
      {isCreateVisible && (
        <BillerForm
          isModalVisible={isCreateVisible}
          onCloseModal={onCloseModal}
          record={rowRecord}
        />
      )}
      <div className="container">
        <PageHeader
          pageTitle="Биллер жагсаалт"
          pageButtons={{
            dataExportName: "Биллер жагсаалт",
            dataSource: dataSource, //export excel data
            columns: columns, // action search
            onCreate: onCreate, //pass action function else false,
          }}
        />
        <Table
          bordered
          size="small"
          dataSource={dataSource}
          columns={columns}
          onRow={(record) => {
            return {
              onDoubleClick: () => {
                onHandleDoubleClick(record);
              }, // mouse enter row
            };
          }}
          pagination={{
            position: ["bottomRight"],
          }}
        />
      </div>
    </>
  );
};

export default Biller;
