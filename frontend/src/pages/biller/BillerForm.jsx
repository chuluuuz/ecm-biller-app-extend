import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { Modal, Button, Form, Input, Row, DatePicker } from "antd";
import axiosInstance from "../../api/axios";
import dayjs from "dayjs";
const BillerForm = ({ isModalVisible, onCloseModal, record }) => {
  const [modalVisible, setModalVisible] = useState(isModalVisible);
  const [form] = Form.useForm();

  useEffect(() => {
    if (Object?.keys(record)?.length > 0) {
      form.setFieldsValue({
        ...record,
        start_date: dayjs(record.start_date),
        end_date: dayjs(record.end_date),
      });
    }
  }, [modalVisible]);
  const handleCancel = () => {
    form.resetFields(); // Reset form fields
    setModalVisible(false); // Hide modal
    onCloseModal && onCloseModal();
  };
  const onHandleSave = async () => {
    const validatedFiels = await form.validateFields();
    if (!validatedFiels?.errorFields) {
      try {
        if (Object?.keys(record)?.length > 0) {
          const result = await axiosInstance.put(
            `/api/biller/${record.id}`,
            validatedFiels
          );

          if (result.status === 200) {
            onCloseModal(true);
          }
        } else {
          const result = await axiosInstance.post(
            "/api/biller",
            validatedFiels
          );
          if (result.status === 201) {
            onCloseModal(true);
          }
        }
      } catch (error) {
        console.error(error);
      }
    }
  };
  return (
    <Modal
      title={`Биллер ${record ? "засварлах" : "үүсгэх"}`}
      open={modalVisible}
      width={"560px"}
      height={"400px"}
      onCancel={handleCancel}
      footer={[
        <Button key="cancel" onClick={handleCancel}>
          Болих
        </Button>,
        <Button
          key="save"
          htmlType="submit"
          onClick={onHandleSave}
          type="primary"
        >
          {"Хадгалах"}
        </Button>,
      ]}
    >
      <Form form={form} layout="vertical">
        <Row gutter={16} style={{ gap: "20px" }}>
          <Form.Item
            label="Биллер код"
            name="biller_code"
            rules={[{ required: true, message: "" }]}
          >
            <Input size="small" key="biller_code" autoFocus />
          </Form.Item>
          <Form.Item
            label="Байгуулга"
            name="company_name"
            rules={[{ required: true, message: "" }]}
          >
            <Input size="small" key="company_name" autoFocus />
          </Form.Item>
          <Form.Item
            label="Бүтээгдэхүүн"
            name="product"
            rules={[{ required: true, message: "" }]}
          >
            <Input size="small" key="biller_code" autoFocus />
          </Form.Item>
        </Row>
        <Row gutter={16} style={{ gap: "20px" }}>
          <Form.Item
            label="Эхлэсэн огноо"
            name="start_date"
            rules={[{ required: true, message: "" }]}
          >
            <DatePicker size="small" />
          </Form.Item>
          <Form.Item
            label="Дуусах огноо"
            name="end_date"
            rules={[{ required: true, message: "" }]}
          >
            <DatePicker size="small" />
          </Form.Item>
        </Row>
      </Form>
    </Modal>
  );
};
BillerForm.propTypes = {
  isModalVisible: PropTypes.bool,
  onCloseModal: PropTypes.func,
  record: PropTypes.object,
};
export default BillerForm;
