import React, { useState } from "react";
import {
  Button,
  Form,
  Input,
  Flex,
  Col,
  Spin,
  Row,
  Card,
  Typography,
} from "antd";
import { LoadingOutlined, UserOutlined, LockOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import axiosInstance from "../../api/axios";
const Login = () => {
  const { Title } = Typography;

  const [form] = Form.useForm();
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const onFinish = async (values) => {
    try {
      if (values?.username?.length > 0 && values?.password?.length > 0) {
        setIsLoading(true);
        const result = await axiosInstance.post("/api/token/", {
          username: values?.username,
          password: values?.password,
        });
        if (result.status == 200) {
          localStorage.setItem("token", result.data.access);
          localStorage.setItem("refresh_token", result.data.refresh);
          navigate("/");
          console.log("result", result.data);
          setIsLoading(false);
        }
      }
    } catch (error) {
      setIsLoading(false);
      console.error(error);
    }
  };
  return (
    <Flex
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        backgroundColor: "#1677ff",
      }}
    >
      <Card
        style={{ width: 500, height: 400, justifyContent: "space-between" }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "center",

            marginBottom: "50px",
          }}
        >
          <Title level={2}> </Title>
          <div
            style={{
              backgroundImage: `url('/assets/company.png')`,
              backgroundSize: "cover",
              width: "300px",
              height: "80px",
              backgroundRepeat: "no-repeat",
            }}
          />
        </div>
        <Row align={"middle"} justify={"center"}>
          <Form
            form={form}
            layout="vertical"
            onFinish={onFinish}
            autoComplete="off"
            disabled={isLoading ? true : false}
          >
            <Col>
              <Col>
                <Form.Item
                  name="username"
                  rules={[
                    {
                      required: true,
                      message: "Нэвтрэх нэрээ оруулна уу",
                    },
                  ]}
                >
                  <Input
                    prefix={<UserOutlined className="site-form-item-icon" />}
                    placeholder="Нэвтрэх нэрээ оруулна уу"
                    size="large"
                  />
                </Form.Item>
              </Col>
              <Col>
                <Form.Item
                  //label="Нууц үг"
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: "",
                    },
                  ]}
                >
                  <Input.Password
                    prefix={<LockOutlined />}
                    size="large"
                    placeholder="Нууц үгээ оруулна уу"
                  />
                </Form.Item>
              </Col>

              <Button
                type="primary"
                htmlType="submit"
                disabled={isLoading}
                size="large"
                block
              >
                {isLoading ? (
                  <Spin indicator={<LoadingOutlined />} />
                ) : (
                  "НЭВТРЭХ"
                )}
              </Button>
            </Col>
          </Form>
        </Row>
      </Card>
    </Flex>
  );
};

export default Login;
