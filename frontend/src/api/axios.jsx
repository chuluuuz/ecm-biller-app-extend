import axios from "axios";

// export const baseURL = process.env.VITE_API;

export const baseURL = import.meta.env.VITE_API;
const axiosInstance = axios.create({
  baseURL: baseURL,
  headers: {
    "Content-Type": "application/json",
  },
});
axiosInstance.interceptors.request.use(
  async (config) => {
    const token = localStorage.getItem("token");
    if ((token || "")?.trim() !== "") {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  function (response) {
    if (response?.data?.code === 401 || response?.data?.code === 403) {
      localStorage.removeItem("token");
      window.location.href = "/";
    }

    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default axiosInstance;
