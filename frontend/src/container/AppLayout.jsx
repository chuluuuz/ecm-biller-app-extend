import PropTypes from "prop-types";
import { useState } from "react";
import { Layout, Menu, Button, Col } from "antd";
import {
  DesktopOutlined,
  PieChartOutlined,
  TeamOutlined,
  UserOutlined,
  LogoutOutlined,
} from "@ant-design/icons";

const AppLayout = ({ children }) => {
  const { Content, Footer, Sider } = Layout;
  const [collapsed, setCollapsed] = useState(false);

  function getItem(label, key, icon, children) {
    return {
      key,
      icon,
      children,
      label,
    };
  }
  const items = [getItem("Биллер жагсаалт", "1", <PieChartOutlined />)];
  return (
    <Layout style={{ minHeight: "100dvh" }}>
      <Sider
        theme="dark"
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
      >
        <Col
          style={{
            height: "100%",
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
          }}
        >
          <Menu
            theme="dark"
            defaultSelectedKeys={["1"]}
            mode="inline"
            items={items}
          ></Menu>
          <Button
            style={{ margin: "20px" }}
            title={"Системээс гарах"}
            size="small"
            ghost
            type="default"
            onClick={() => {
              localStorage.clear();
              window.location.reload();
            }}
            icon={<LogoutOutlined />}
          >
            {!collapsed ? "Системээс гарах" : ""}
          </Button>
        </Col>
      </Sider>
      <Layout>
        <Content>{children}</Content>
        <Footer style={{ textAlign: "center" }}>ECM BILLING LLC</Footer>
      </Layout>
    </Layout>
  );
};
AppLayout.propTypes = {
  children: PropTypes.node.isRequired,
};
export default AppLayout;
