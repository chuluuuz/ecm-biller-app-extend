from django.urls import path
from .views import ECMBillerView,ECMBillerDetailView
urlpatterns = [
    path('biller',ECMBillerView.as_view(),),
    path('biller/<int:id>', ECMBillerDetailView.as_view()),
]