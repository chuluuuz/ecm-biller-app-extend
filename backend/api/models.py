from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ECMBiller(models.Model):

    biller_code = models.CharField(max_length=10,default="",unique=True,)
    company_name = models.CharField(max_length=30,default="",)
    product = models.CharField(max_length=30,default='')
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_user = models.ForeignKey(User,  on_delete=models.CASCADE,related_name='created_ecmbillers',null=True)
    updated_user = models.ForeignKey(User, on_delete=models.CASCADE,related_name='updated_ecmbillers',null=True)

    class Meta:
        ordering = ['end_date']