from django.shortcuts import render
from rest_framework import generics,status
from .serializers import ECMBillerSerializer
from .models import ECMBiller
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView
from rest_framework.response import Response

class ECMBillerView(APIView):
    serializer_class = ECMBillerSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        serializer = ECMBillerSerializer(ECMBiller.objects.all(), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
        
    
    def post(self, request, *args, **kwargs):
        serializer = ECMBillerSerializer(data= request.data)
        if serializer.is_valid():
            serializer.save(created_user=self.request.user,updated_user=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ECMBillerDetailView(APIView):
    permission_classes = [IsAuthenticated]

    def get_object(self, id):
        '''
        Helper method to get the object with given todo_id, and user_id
        '''
        try:
            return ECMBiller.objects.get(id=id)
        except ECMBiller.DoesNotExist:
            return None
        
    def get(self, request, id, *args, **kwargs):
        '''
        Retrieves the Todo with given todo_id
        '''
        biller_instance = self.get_object(id)
        if not biller_instance:
            return Response(
                {"res": "Object with Biller id does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = ECMBillerSerializer(biller_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    def put(self, request, id, *args, **kwargs):
        '''
        Updates the todo item with given todo_id if exists
        '''
        biller_instance = self.get_object(id)
        if not biller_instance:
            return Response(
                {"res": "Object with Biller id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        
        serializer = ECMBillerSerializer(instance = biller_instance, data=request.data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self, request, id, *args, **kwargs):
        '''
        Deletes the todo item with given todo_id if exists
        '''
        biller_instance = self.get_object(id)
        if not biller_instance:
            return Response(
                {"res": "Object with Biller id does not exists"}, 
                status=status.HTTP_400_BAD_REQUEST
            )
        biller_instance.delete()
        return Response(
            {"res": "Object deleted!"},
            status=status.HTTP_200_OK
        )