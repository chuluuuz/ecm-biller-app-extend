from rest_framework import serializers
from .models import ECMBiller

class ECMBillerSerializer(serializers.ModelSerializer):
    class Meta:
        model = ECMBiller
        fields = ('id','biller_code','company_name','product','start_date','end_date','created_at','updated_at','created_user','updated_user')

# class CreateECMBillerSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = ECMBiller
#         fields = ('id','biller_code','company_name','product','start_date','end_date')